var stompClient = null;
var socket;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    // socket = new WebSocket('ws://localhost:8080/domain-data-server/base-station/')
    // socket.binaryType = "arraybuffer";
    // socket.onopen = function (event) {
    //     exampleSocket.send("connected by client");
    // };
    //
    // socket.onmessage = function (evt) {
    //     console.log(evt.data, "Message")
    // }
    //
    // socket.onerror = function (evt) {
    //     console.log(evt.returnValue, "error")
    // }
    var socket = new SockJS('http://localhost:8080/websock');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        
        stompClient.subscribe('/stomp/mobile/a98406fe-3ad8-4912-97d9-ab7422254005', {"Authorization": "Bearer token", "contentType": "json/application"}, function (greeting) {
            console.log("here")
            console.log(greeting);
            showGreeting(greeting);
        });

    })
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    function sendText() {
        var msg = {
            type: "message",
            text: document.getElementById("text").value,
            id:   clientID,
            date: Date.now()
        };

        // Send the msg object as a JSON-formatted string.
        socket.send(JSON.stringify(msg));

    }
    if(socket != undefined) {

    }
    // stompClient.send("/app/save-vital/mobile/a98406fe-3ad8-4912-97d9-422254005", {}, JSON.stringify({
    //     "infantId": "a98406fe-3ad8-4912-97d9-ab7422254005",
    //     "generatedTime": "1560713267",
    //     "heartRate": 102.2,
    //     "spo2Level":80.2,
    //     "bodyTemperature": 98.6,
    //     "roomTemperature": 69.3,
    //     "respiratoryRate": 98.6,
    //     "humidityLevel": 65.3,
    //     "roomAirQualityIndex": 56.3
    // }));

    stompClient.send("/app/save-vital/mobile/error/a98406fe-3ad8-4912-97d9-422254005", {}, JSON.stringify({
        "infantId": "a98406fe-3ad8-4912-97d9-ab7422254005",
        "generatedTime": "1560713267",
        "heartRate": 102.2,
        "spo2Level":80.2,
        "bodyTemperature": 98.6,
        "roomTemperature": 69.3,
        "respiratoryRate": 98.6,
        "humidityLevel": 65.3,
        "roomAirQualityIndex": 56.3
    }));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});
